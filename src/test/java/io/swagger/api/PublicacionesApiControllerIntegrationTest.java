package io.swagger.api;

import io.swagger.model.ErrorList;
import io.swagger.model.GetPublicacioensOUT;
import java.util.UUID;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PublicacionesApiControllerIntegrationTest {

    @Autowired
    private PublicacionesApi api;

    @Test
    public void publicacionesGetTest() throws Exception {
        UUID xRequestID = new UUID(0L,0L);
        String xChannel = "xChannel_example";
        String xClientID = "xClientID_example";
        ResponseEntity<GetPublicacioensOUT> responseEntity = api.publicacionesGet(xRequestID, xChannel, xClientID);
        assertEquals(HttpStatus.NOT_IMPLEMENTED, responseEntity.getStatusCode());
    }

}
