package io.swagger.model;

import java.util.Objects;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonValue;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Códigos de mensaje definidos en O.B Engine para código de error HTTP 409 (CONFLICT).
 */
public enum MessageCode409OB {
  INVALID("STATUS_INVALID");

  private String value;

  MessageCode409OB(String value) {
    this.value = value;
  }

  @Override
  @JsonValue
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static MessageCode409OB fromValue(String text) {
    for (MessageCode409OB b : MessageCode409OB.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
}
