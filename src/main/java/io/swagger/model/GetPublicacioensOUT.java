package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.GetPublicacioensComplete;
import io.swagger.model.Result;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Salida OK
 */
@ApiModel(description = "Salida OK")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-09-15T12:01:47.709Z[GMT]")
public class GetPublicacioensOUT   {
  @JsonProperty("result")
  private Result result = null;

  @JsonProperty("body")
  private GetPublicacioensComplete body = null;

  public GetPublicacioensOUT result(Result result) {
    this.result = result;
    return this;
  }

  /**
   * Get result
   * @return result
  **/
  @ApiModelProperty(value = "")

  @Valid
  public Result getResult() {
    return result;
  }

  public void setResult(Result result) {
    this.result = result;
  }

  public GetPublicacioensOUT body(GetPublicacioensComplete body) {
    this.body = body;
    return this;
  }

  /**
   * Get body
   * @return body
  **/
  @ApiModelProperty(value = "")

  @Valid
  public GetPublicacioensComplete getBody() {
    return body;
  }

  public void setBody(GetPublicacioensComplete body) {
    this.body = body;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetPublicacioensOUT getPublicacioensOUT = (GetPublicacioensOUT) o;
    return Objects.equals(this.result, getPublicacioensOUT.result) &&
        Objects.equals(this.body, getPublicacioensOUT.body);
  }

  @Override
  public int hashCode() {
    return Objects.hash(result, body);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetPublicacioensOUT {\n");
    
    sb.append("    result: ").append(toIndentedString(result)).append("\n");
    sb.append("    body: ").append(toIndentedString(body)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
