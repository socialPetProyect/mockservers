package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.ErrorMessage404OB;
import io.swagger.model.ErrorMessageCategory;
import io.swagger.model.MessageCode404OB;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ErrorMessage404OB
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-09-15T12:01:47.709Z[GMT]")
public class ErrorMessage404OB   {
  @JsonProperty("category")
  private ErrorMessageCategory category = null;

  @JsonProperty("code")
  private MessageCode404OB code = null;

  @JsonProperty("path")
  private String path = null;

  @JsonProperty("text")
  private ErrorMessage404OB text = null;

  public ErrorMessage404OB category(ErrorMessageCategory category) {
    this.category = category;
    return this;
  }

  /**
   * Get category
   * @return category
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
  public ErrorMessageCategory getCategory() {
    return category;
  }

  public void setCategory(ErrorMessageCategory category) {
    this.category = category;
  }

  public ErrorMessage404OB code(MessageCode404OB code) {
    this.code = code;
    return this;
  }

  /**
   * Get code
   * @return code
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
  public MessageCode404OB getCode() {
    return code;
  }

  public void setCode(MessageCode404OB code) {
    this.code = code;
  }

  public ErrorMessage404OB path(String path) {
    this.path = path;
    return this;
  }

  /**
   * Get path
   * @return path
  **/
  @ApiModelProperty(value = "")

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public ErrorMessage404OB text(ErrorMessage404OB text) {
    this.text = text;
    return this;
  }

  /**
   * Get text
   * @return text
  **/
  @ApiModelProperty(value = "")

  @Valid
  public ErrorMessage404OB getText() {
    return text;
  }

  public void setText(ErrorMessage404OB text) {
    this.text = text;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErrorMessage404OB errorMessage404OB = (ErrorMessage404OB) o;
    return Objects.equals(this.category, errorMessage404OB.category) &&
        Objects.equals(this.code, errorMessage404OB.code) &&
        Objects.equals(this.path, errorMessage404OB.path) &&
        Objects.equals(this.text, errorMessage404OB.text);
  }

  @Override
  public int hashCode() {
    return Objects.hash(category, code, path, text);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorMessage404OB {\n");
    
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    path: ").append(toIndentedString(path)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
