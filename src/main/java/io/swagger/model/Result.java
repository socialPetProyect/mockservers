package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Result
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-09-15T12:01:47.709Z[GMT]")
public class Result   {
  @JsonProperty("code")
  private String code = null;

  @JsonProperty("info")
  private String info = null;

  public Result code(String code) {
    this.code = code;
    return this;
  }

  /**
   * Código retornado por el core. 
   * @return code
  **/
  @ApiModelProperty(example = "INF003", required = true, value = "Código retornado por el core. ")
  @NotNull

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Result info(String info) {
    this.info = info;
    return this;
  }

  /**
   * Mensaje más detalla y legible que describe el código retornado por la ejecuión del servicio. 
   * @return info
  **/
  @ApiModelProperty(example = "Mensaje informativo OK", required = true, value = "Mensaje más detalla y legible que describe el código retornado por la ejecuión del servicio. ")
  @NotNull

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Result result = (Result) o;
    return Objects.equals(this.code, result.code) &&
        Objects.equals(this.info, result.info);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, info);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Result {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    info: ").append(toIndentedString(info)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
