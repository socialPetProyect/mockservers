package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.ErrorMessage409OB;
import io.swagger.model.ErrorMessageCategory;
import io.swagger.model.MessageCode409OB;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ErrorMessage409OB
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-09-15T12:01:47.709Z[GMT]")
public class ErrorMessage409OB   {
  @JsonProperty("category")
  private ErrorMessageCategory category = null;

  @JsonProperty("code")
  private MessageCode409OB code = null;

  @JsonProperty("path")
  private String path = null;

  @JsonProperty("text")
  private ErrorMessage409OB text = null;

  public ErrorMessage409OB category(ErrorMessageCategory category) {
    this.category = category;
    return this;
  }

  /**
   * Get category
   * @return category
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
  public ErrorMessageCategory getCategory() {
    return category;
  }

  public void setCategory(ErrorMessageCategory category) {
    this.category = category;
  }

  public ErrorMessage409OB code(MessageCode409OB code) {
    this.code = code;
    return this;
  }

  /**
   * Get code
   * @return code
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid
  public MessageCode409OB getCode() {
    return code;
  }

  public void setCode(MessageCode409OB code) {
    this.code = code;
  }

  public ErrorMessage409OB path(String path) {
    this.path = path;
    return this;
  }

  /**
   * Get path
   * @return path
  **/
  @ApiModelProperty(value = "")

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public ErrorMessage409OB text(ErrorMessage409OB text) {
    this.text = text;
    return this;
  }

  /**
   * Get text
   * @return text
  **/
  @ApiModelProperty(value = "")

  @Valid
  public ErrorMessage409OB getText() {
    return text;
  }

  public void setText(ErrorMessage409OB text) {
    this.text = text;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ErrorMessage409OB errorMessage409OB = (ErrorMessage409OB) o;
    return Objects.equals(this.category, errorMessage409OB.category) &&
        Objects.equals(this.code, errorMessage409OB.code) &&
        Objects.equals(this.path, errorMessage409OB.path) &&
        Objects.equals(this.text, errorMessage409OB.text);
  }

  @Override
  public int hashCode() {
    return Objects.hash(category, code, path, text);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorMessage409OB {\n");
    
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    path: ").append(toIndentedString(path)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
