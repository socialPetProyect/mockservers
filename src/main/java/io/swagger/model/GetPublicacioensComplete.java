package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.LocalDate;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * GetPublicacioensCompleteaa
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-09-15T12:01:47.709Z[GMT]")
public class GetPublicacioensComplete   {
  @JsonProperty("tituloPublicacion")
  private String tituloPublicacion = null;

  @JsonProperty("publicacionIMG")
  private String publicacionIMG = null;

  @JsonProperty("typeDate")
  private LocalDate typeDate = null;

  @JsonProperty("descriPublicacion")
  private String descriPublicacion = null;

  public GetPublicacioensComplete tituloPublicacion(String tituloPublicacion) {
    this.tituloPublicacion = tituloPublicacion;
    return this;
  }

  /**
   * Get tituloPublicacion
   * @return tituloPublicacion
  **/
  @ApiModelProperty(value = "")

@Pattern(regexp="[A-Z-a-z]") @Size(min=1,max=50)   public String getTituloPublicacion() {
    return tituloPublicacion;
  }

  public void setTituloPublicacion(String tituloPublicacion) {
    this.tituloPublicacion = tituloPublicacion;
  }

  public GetPublicacioensComplete publicacionIMG(String publicacionIMG) {
    this.publicacionIMG = publicacionIMG;
    return this;
  }

  /**
   * Get publicacionIMG
   * @return publicacionIMG
  **/
  @ApiModelProperty(value = "")

@Pattern(regexp="[A-Za-z][0-9]") @Size(min=1,max=400)   public String getPublicacionIMG() {
    return publicacionIMG;
  }

  public void setPublicacionIMG(String publicacionIMG) {
    this.publicacionIMG = publicacionIMG;
  }

  public GetPublicacioensComplete typeDate(LocalDate typeDate) {
    this.typeDate = typeDate;
    return this;
  }

  /**
   * Get typeDate
   * @return typeDate
  **/
  @ApiModelProperty(value = "")

  @Valid
@Size(max=10)   public LocalDate getTypeDate() {
    return typeDate;
  }

  public void setTypeDate(LocalDate typeDate) {
    this.typeDate = typeDate;
  }

  public GetPublicacioensComplete descriPublicacion(String descriPublicacion) {
    this.descriPublicacion = descriPublicacion;
    return this;
  }

  /**
   * Get descriPublicacion
   * @return descriPublicacion
  **/
  @ApiModelProperty(value = "")

@Pattern(regexp="[A-Za-z][0-9]") @Size(min=1,max=800)   public String getDescriPublicacion() {
    return descriPublicacion;
  }

  public void setDescriPublicacion(String descriPublicacion) {
    this.descriPublicacion = descriPublicacion;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetPublicacioensComplete getPublicacioensComplete = (GetPublicacioensComplete) o;
    return Objects.equals(this.tituloPublicacion, getPublicacioensComplete.tituloPublicacion) &&
        Objects.equals(this.publicacionIMG, getPublicacioensComplete.publicacionIMG) &&
        Objects.equals(this.typeDate, getPublicacioensComplete.typeDate) &&
        Objects.equals(this.descriPublicacion, getPublicacioensComplete.descriPublicacion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(tituloPublicacion, publicacionIMG, typeDate, descriPublicacion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetPublicacioensComplete {\n");
    
    sb.append("    tituloPublicacion: ").append(toIndentedString(tituloPublicacion)).append("\n");
    sb.append("    publicacionIMG: ").append(toIndentedString(publicacionIMG)).append("\n");
    sb.append("    typeDate: ").append(toIndentedString(typeDate)).append("\n");
    sb.append("    descriPublicacion: ").append(toIndentedString(descriPublicacion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
