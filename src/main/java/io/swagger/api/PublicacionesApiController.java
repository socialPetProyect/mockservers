package io.swagger.api;

import io.swagger.model.ErrorList;
import io.swagger.model.GetPublicacioensOUT;
import java.util.UUID;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-09-15T12:01:47.709Z[GMT]")
@Controller
public class PublicacionesApiController implements PublicacionesApi {

    private static final Logger log = LoggerFactory.getLogger(PublicacionesApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public PublicacionesApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<GetPublicacioensOUT> publicacionesGet(@ApiParam(value = "Identificador único universal que identifica la petición de forma única." ,required=true) @RequestHeader(value="X-Request-ID", required=true) UUID xRequestID,@ApiParam(value = "Canal desde el que se origina la petición." ,required=true) @RequestHeader(value="X-Channel", required=true) String xChannel,@ApiParam(value = "Número de persona en O.B Engine sobre el que se realiza la petición." ) @RequestHeader(value="X-Client-ID", required=false) String xClientID) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<GetPublicacioensOUT>(objectMapper.readValue("{\n  \"result\" : {\n    \"code\" : \"INF003\",\n    \"info\" : \"Mensaje informativo OK\"\n  },\n  \"body\" : {\n    \"typeDate\" : \"2025-05-12T00:00:00.000+0000\",\n    \"publicacionIMG\" : \"imagen\",\n    \"tituloPublicacion\" : \"Titulo de la publicación\",\n    \"descriPublicacion\" : \"publicación\"\n  }\n}", GetPublicacioensOUT.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<GetPublicacioensOUT>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<GetPublicacioensOUT>(HttpStatus.NOT_IMPLEMENTED);
    }

}
