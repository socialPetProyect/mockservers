/**
 * NOTE: This class is auto generated by the swagger code generator program (3.0.11).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package io.swagger.api;

import io.swagger.model.ErrorList;
import io.swagger.model.GetPublicacioensOUT;
import java.util.UUID;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-09-15T12:01:47.709Z[GMT]")
@Api(value = "publicaciones", description = "the publicaciones API")
public interface PublicacionesApi {

    @ApiOperation(value = "CRUD Publicaciones", nickname = "publicacionesGet", notes = "Consulta de todas las publicaciones", response = GetPublicacioensOUT.class, tags={ "API Publicaciones", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Salida OK de alta de un préstamo.", response = GetPublicacioensOUT.class),
        @ApiResponse(code = 400, message = "Bad Request", response = ErrorList.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = ErrorList.class),
        @ApiResponse(code = 403, message = "Forbidden", response = ErrorList.class),
        @ApiResponse(code = 404, message = "Not found", response = ErrorList.class),
        @ApiResponse(code = 500, message = "Internal Server Error") })
    @RequestMapping(value = "/publicaciones",
        produces = { "application/json", "application/problem+json" }, 
        method = RequestMethod.GET)
    ResponseEntity<GetPublicacioensOUT> publicacionesGet(@ApiParam(value = "Identificador único universal que identifica la petición de forma única." ,required=true) @RequestHeader(value="X-Request-ID", required=true) UUID xRequestID,@ApiParam(value = "Canal desde el que se origina la petición." ,required=true) @RequestHeader(value="X-Channel", required=true) String xChannel,@ApiParam(value = "Número de persona en O.B Engine sobre el que se realiza la petición." ) @RequestHeader(value="X-Client-ID", required=false) String xClientID);

}
